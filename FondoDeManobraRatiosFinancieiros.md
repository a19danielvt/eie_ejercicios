# Ejercicio 1

# Activo -> 2350

- Corrente
  - produtos terminados  300    | -> existencias (480)
  - materias primas 100         |
  - clientes    80                          |
  - efectos para cobrar 150                 |-> realizable (300)
  - investimentos financieiros a c/p    70  |
  - caixa   50  |-> disponible (150)
  - bancos 100  |
    - 850

- Non corrente
  - maquinaria  500
  - elementos de trasporte  150
  - constucións 850
  - terreos 200
  - amortización acumulada  200 (resta)
    - 1500

# Pasivo e Neto -> 2350

- Corrente
  - provedores  215
  - facenda pública, acredora   40
  - préstamos a c/p 160
  - acredores a c/p 100
    - 515

- Non corrente
  - préstamos a l/p 425
    - 425

- Neto
- capital social  1200
- reservas    210
  - 1410


Fondo de maniobra = AC - PC |
FM = 850 - 515              |-> dos formas de calcularlo
FM = 335                    |
                            |-> da positivo, por lo que tiene 
FM = (Neto + PNC) - ANC     |   estabilidad financiera
FM = (1410 + 425) - 1500    |
FM = 335                    |

Ratio liquidez = AC / PC    |
RL = 850 / 515              |-> entre 1,5 y 2 no existen problemas de liquidez en la empresa
RL = 1,65                   |

Ratio Tesouraría = (Disponible + Realizable) / PC   |
RT = (150 + 300) / 515                              |-> entre 0,75 y 1 es valor de equilibrio
RT = 0,87                                           |

Ratio Disponibilidad = disponible / PC  |
RD = 150 / 515                          |-> entre 0,1 y 0,3 es valor de equilibrio
RD = 0,29                               |

Ratio Garantía = Activo total / Pasivo total    |-> entre 1,5 y 2,5
RG = 2350 / 940                                 |-> si se acerca a 1, se arpoxima la quiebra
RG = 2,5                                        |-> si es < 1, la empresa está en quiebra técnica

Ratio Endeudamiento = Pasivo total / (Neto + Pasivo)    |-> no debe ser superior a 0,5, lo que 
RE = 940 / (1410 + 940)                                 |   significaría que las deudas superan 
RE = 0,4                                                |   los fondos propios
                                                        |
RE = PT / Neto                                          |-> de esta otra forma indica la cantidad
RE = 940 / 1410                                         |   de fondos ajenos que utiliza la empresa
RE = 0,66                                               |-> mayor que 1 indica exceso de deuda

Ratio Autonomía Financieira = Neto / Pasivo Total   |-> cuanto más alto, la empresa será menos
RAF = 1410 / 940                                    |   dependiente de sus acreedores
RAF = 1,5                                           |-> inverso de RE


# Ejercicio 2

# Activo -> 180.400

- Corriente
  - existencias     77.500
  - clientes        41.100
  - caixa e bancos  16.800
    - 135.400


- No corriente
  - terreos         5.000
  - mobiliario      17.500
  - edificios       22.500
    - 45.000


# Pasivo y Neto -> 180.400

- Corriente
  - provedores      32.500
    - 32.500


- No corriente
  - débedas a L/P   49.000
    - 49.000

- Neto
  - reservas        46.100  
  - capital         52.800
    - 98.900


FM = 135.400 - 32.500
FM = 102.900


# Ejercicio 3

capital social      1200
reservas            600
débedas a L/P       500
débedas a C/P       700
total neto e pasivo 3.000

RE = PT / (N + PT)
RE = 1200 / (1800 + 1200)
RE = 0,4


# Ejercicio 4

# Activo

- Corriente
  - existencias         260.000
  - clientes            250.000
  - caixa e bancos      10.000
    - 520.000

- No corriente
  - inmobilizado        490.000
  - amortizacións acum  150.000 -> resta
    - 340.000


# Pasivo

- Corriente
  - acredores a C/P     360.000
  - provedores          100.000
    - 460.000


- No corriente
  - débedas a L/P       200.000
    - 200.000



- Neto
  - reservas            100.000
  - capital social      100.000
    - 200.000
