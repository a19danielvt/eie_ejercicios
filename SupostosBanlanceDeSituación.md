#### **Activo** 180.000

- **Corrente**
  - Existencias 38.000
  - Outro activo corrente  1.500
  - Clientes  19.000
  - Deudores varios  500
  - Inversións especulativas a C/P  5.000
  - Tesouraría  3.500
    - 67.500

- **Non corrente**
  - Instalacións técnicas  60.000
  - Terreos e construcións  30.000
  - Patentes, licencias e marcas  400
  - Aplicacións infromáticas  500
  - Anticipos a provedores L/P  500
  - Créditos a terceiros a L/P 100
  - Inversións en empresas do grupo L/P  20.000
  - Outros activos non correntes  1.000
    - 112.500


#### **Pasivo**

- **Corrente**
  - Débedas con administracións públicas a C/P  3.500
  - Provedores  5.800
  - Acredores varios  6.200
  - Remuneracións pendentes de pago  3.800
  - Outras débedas a C/P  1.000
  - Débedas a C/P con entidades financeiras  1.700
    - 22.000


- **Non corrente**
  - Débedas a L/P con entidades financeiras  22.000
  - Outros pasivos L/P  1.800
    - 23.800


- **Patrimonio Neto**
  - Capital social  5.500
  - Reserva legal  1.000
  - Beneficio do exercicio  X
  - Subvencións de capital  700
  - Reservas voluntarias 122.000
    - 129.200 + X


Activo = Patrimonio Neto + Pasivo
180.000 = 129.200 + X + 45.800
X = 180.000 - 175.000
**X = 5.000** -> Beneficio do exercicio